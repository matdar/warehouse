package pl.sdacademy.warehouse.service.product.command;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sdacademy.warehouse.domain.entity.Product;
import pl.sdacademy.warehouse.repository.ProductRepository;
import pl.sdacademy.warehouse.service.part.command.PartCommandService;
import pl.sdacademy.warehouse.service.product.exception.ProductNotFoundException;

import javax.transaction.Transactional;

@Service
@Transactional
public class ProductCommandService {

    private final Logger LOGGER = LoggerFactory.getLogger(PartCommandService.class);

    private final ProductRepository productRepository;

    @Autowired
    public ProductCommandService (ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public Long saveProduct (Product product){
        productRepository.save(product);

        return product.getId();
    }

    public void update (Product product){
        Product productUpdate = productRepository.findOne(product.getId());
        if(productUpdate==null){
            LOGGER.debug("Part with id " + product.getId() + " not found.");
            throw new ProductNotFoundException();
        }

        productUpdate.setProductName(product.getProductName());
        productUpdate.setProductDescription(product.getProductDescription());
    }

    public void delete (Long id){
        Product product = productRepository.findOne(id);
        if(product==null){
            LOGGER.debug("Part with id " + product.getId() + " not found.");
            throw new ProductNotFoundException();
        }

        productRepository.delete(product);
    }
}
