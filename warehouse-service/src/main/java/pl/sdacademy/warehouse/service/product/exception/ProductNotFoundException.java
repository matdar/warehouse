package pl.sdacademy.warehouse.service.product.exception;

public class ProductNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1369368614263620977L;
}
