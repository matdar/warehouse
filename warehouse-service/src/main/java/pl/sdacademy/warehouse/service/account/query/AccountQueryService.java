package pl.sdacademy.warehouse.service.account.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.Account;
import pl.sdacademy.warehouse.repository.AccountRepository;
import pl.sdacademy.warehouse.service.account.exception.AccountNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class AccountQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountQueryService.class);

    private final AccountRepository accountRepository;

    @Autowired
    public AccountQueryService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Account findById(Long id) {
        Account account = accountRepository.findOne(id);
        if (account == null) {
            LOGGER.debug("Part with id " + id + " not found.");
            throw new AccountNotFoundException();
        }

        return account;
    }


}
