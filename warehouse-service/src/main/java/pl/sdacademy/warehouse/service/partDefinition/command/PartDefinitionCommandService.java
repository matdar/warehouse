package pl.sdacademy.warehouse.service.partDefinition.command;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.Part;
import pl.sdacademy.warehouse.domain.entity.PartDefinition;
import pl.sdacademy.warehouse.repository.PartDefinitionRepository;
import pl.sdacademy.warehouse.repository.PartRepository;
import pl.sdacademy.warehouse.service.part.query.PartQueryService;
import pl.sdacademy.warehouse.service.partDefinition.exception.PartDefinitionNotFoundException;

@Service
@Transactional
public class PartDefinitionCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartQueryService.class);

    private final PartDefinitionRepository partDefinitionRepository;
    private final PartRepository partRepository;

    @Autowired
    public PartDefinitionCommandService (PartDefinitionRepository partDefinitionRepository, PartRepository partRepository){
        this.partDefinitionRepository = partDefinitionRepository;
        this.partRepository = partRepository;
    }

    public Long savePart (PartDefinition partDefinition){
        Part part = new Part(0, partDefinition);
        partDefinition.setPart(part);
        partRepository.save(part);
        partDefinitionRepository.save(partDefinition);

        return partDefinition.getId();
    }

    public void update (PartDefinition partDefinition){
        PartDefinition partDefinitionUpdate = partDefinitionRepository.findOne(partDefinition.getId());
        if(partDefinitionUpdate==null){
            LOGGER.debug("PartDefinition with id " + partDefinition.getId() + " not found.");
            throw new PartDefinitionNotFoundException();
        }

        partDefinitionUpdate.setName(partDefinition.getName());
        partDefinitionUpdate.setSymbol(partDefinition.getSymbol());
        partDefinitionUpdate.setUnit(partDefinition.getUnit());
    }

    public void delete (Long id){
        PartDefinition partDefinition = partDefinitionRepository.findOne(id);
        if(partDefinition==null){
            LOGGER.debug("PartDefinition with id " + partDefinition.getId() + " not found.");
            throw new PartDefinitionNotFoundException();
        }

        partDefinitionRepository.delete(partDefinition);
    }
}
