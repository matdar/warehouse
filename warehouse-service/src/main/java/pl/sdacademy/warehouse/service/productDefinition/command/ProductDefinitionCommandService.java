package pl.sdacademy.warehouse.service.productDefinition.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.ProductDefinition;
import pl.sdacademy.warehouse.repository.ProductDefinitionRepository;
import pl.sdacademy.warehouse.service.productDefinition.exception.ProductDefinitionNotFoundException;

@Service
@Transactional
public class ProductDefinitionCommandService {

    private final Logger LOGGER = LoggerFactory.getLogger(ProductDefinitionCommandService.class);

    private ProductDefinitionRepository productDefinitionRepository;

    @Autowired
    public ProductDefinitionCommandService(ProductDefinitionRepository productDefinitionRepository) {
        this.productDefinitionRepository = productDefinitionRepository;
    }

    public Long saveProductDefinition(ProductDefinition productDefinition) {
        productDefinitionRepository.save(productDefinition);
        return productDefinition.getId();
    }

    public void update(ProductDefinition productDefinition) {
        ProductDefinition productDefUpdate = productDefinitionRepository.findOne(productDefinition.getId());
        if (productDefUpdate == null) {
            LOGGER.debug("Part with id " + productDefinition.getId() + " not found.");
            throw new ProductDefinitionNotFoundException();
        }

        productDefUpdate.setName(productDefinition.getName());
        productDefUpdate.setDescription(productDefinition.getDescription());
//        productDefUpdate.setProductDefParts(productDefinition.getProductDefParts());
    }

    public void delete(Long id) {
        ProductDefinition productDefinition = productDefinitionRepository.findOne(id);
        if (productDefinition == null) {
            LOGGER.debug("Part with id " + productDefinition.getId() + " not found.");
            throw new ProductDefinitionNotFoundException();
        }
        productDefinitionRepository.delete(productDefinition);
    }
}
