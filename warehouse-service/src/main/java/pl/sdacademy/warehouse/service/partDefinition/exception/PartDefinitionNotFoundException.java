package pl.sdacademy.warehouse.service.partDefinition.exception;

public class PartDefinitionNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1369368614263620977L;
}
