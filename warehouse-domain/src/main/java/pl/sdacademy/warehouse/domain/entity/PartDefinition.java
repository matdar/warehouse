package pl.sdacademy.warehouse.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "part_definition")
public class PartDefinition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "part_definition_id")
    private Long id;

    @Column(name = "part_definition_name")
    private String name;

    @Column(name = "part_definition_symbol")
    private String symbol;

    @Column(name = "part_definition_unit")
    private String unit;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "part_id", nullable = false)
    private Part part;

    public PartDefinition() {
    }

    public PartDefinition(Part part, String name, String symbol, String unit) {
        this.name = name;
        this.symbol = symbol;
        this.unit = unit;
        this.part = part;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }
}
