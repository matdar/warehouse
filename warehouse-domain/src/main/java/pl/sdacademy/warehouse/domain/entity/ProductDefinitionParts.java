package pl.sdacademy.warehouse.domain.entity;

import javax.persistence.*;

@Entity
@Table(name="product_definition_parts")
public class ProductDefinitionParts {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="product_definition_parts_id")
    private ProductDefinitionPartsId productDefinitionPartsId;

    @Column(name="quantity")
    private String quantity;

    public ProductDefinitionParts() {
    }

    public ProductDefinitionParts(ProductDefinitionPartsId productDefinitionPartsId, String quantity) {
        this.productDefinitionPartsId = productDefinitionPartsId;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductDefinitionPartsId getProductDefinitionPartsId() {
        return productDefinitionPartsId;
    }

    public void setProductDefinitionPartsId(ProductDefinitionPartsId productDefinitionPartsId) {
        this.productDefinitionPartsId = productDefinitionPartsId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
