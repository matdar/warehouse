package pl.sdacademy.warehouse.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "product_definition")
public class ProductDefinition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long id;

    @Column(name = "product_def_name")
    private String name;

    @Column(name = "product_def_description")
    private String description;

    public ProductDefinition() {
    }

    public ProductDefinition(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
