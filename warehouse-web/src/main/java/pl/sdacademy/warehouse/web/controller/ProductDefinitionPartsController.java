package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sdacademy.warehouse.domain.entity.PartDefinition;
import pl.sdacademy.warehouse.domain.entity.ProductDefinition;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionParts;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionPartsId;
import pl.sdacademy.warehouse.service.partDefinition.query.PartDefinitionQueryService;
import pl.sdacademy.warehouse.service.productDefinition.query.ProductDefinitionQueryService;
import pl.sdacademy.warehouse.service.productDefinitionParts.command.ProductDefinitionPartsCommandService;
import pl.sdacademy.warehouse.service.productDefinitionParts.query.ProductDefinitionPartsQueryService;

import javax.validation.Valid;

@Controller
public class ProductDefinitionPartsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDefinitionPartsController.class);

    private final ProductDefinitionPartsCommandService productDefinitionPartsCommandService;

    private final ProductDefinitionPartsQueryService productDefinitionPartsQueryService;

    private final PartDefinitionQueryService partDefinitionQueryService;

    private final ProductDefinitionQueryService productDefinitionQueryService;



    @Autowired
    public ProductDefinitionPartsController(ProductDefinitionPartsCommandService productDefinitionPartsCommandService, ProductDefinitionPartsQueryService productDefinitionPartsQueryService, PartDefinitionQueryService partDefinitionQueryService, ProductDefinitionQueryService productDefinitionQueryService) {
        this.productDefinitionPartsCommandService = productDefinitionPartsCommandService;
        this.productDefinitionPartsQueryService = productDefinitionPartsQueryService;
        this.partDefinitionQueryService = partDefinitionQueryService;
        this.productDefinitionQueryService = productDefinitionQueryService;
    }

    @RequestMapping(value = "/productDefinition/addPart")
    public String savePartToProduct(@Valid @ModelAttribute("addPart") ProductDefinitionParts productDefinitionParts,
                                    BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        String result = "redirect:/productDefinition/partsToAdd/" + productDefinitionParts
                .getProductDefinitionPartsId()
                .getProductDefinition()
                .getId();

        LOGGER.debug("is executed!");
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "productDefinitionParts", bindingResult);
            redirectAttributes.addFlashAttribute("flash" + "productDefinitionParts", productDefinitionParts);

            return result;
        }

        if (productDefinitionParts.getId() == null) {
            productDefinitionPartsCommandService.saveProductDefinitionParts(productDefinitionParts);
        } else {
            productDefinitionPartsCommandService.updateProductDefinitionParts(productDefinitionParts);
        }

        return result;
    }
}