package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sdacademy.warehouse.domain.entity.ProductDefinition;
import pl.sdacademy.warehouse.service.partDefinition.query.PartDefinitionQueryService;
import pl.sdacademy.warehouse.service.productDefinition.command.ProductDefinitionCommandService;
import pl.sdacademy.warehouse.service.productDefinition.query.ProductDefinitionQueryService;
import pl.sdacademy.warehouse.service.productDefinitionParts.query.ProductDefinitionPartsQueryService;

import javax.validation.Valid;

@Controller
public class ProductDefinitonController {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDefinitonController.class);

    private final ProductDefinitionQueryService productDefinitionQueryService;
    private final ProductDefinitionCommandService productDefinitionCommandService;
    private final PartDefinitionQueryService partDefinitionQueryService;
    private final ProductDefinitionPartsQueryService productDefinitionPartsQueryService;

    @Autowired
    public ProductDefinitonController(ProductDefinitionQueryService productDefinitionQueryService, ProductDefinitionCommandService productDefinitionCommandService, PartDefinitionQueryService partDefinitionQueryService, ProductDefinitionPartsQueryService productDefinitionPartsQueryService) {
        this.productDefinitionQueryService = productDefinitionQueryService;
        this.productDefinitionCommandService = productDefinitionCommandService;
        this.partDefinitionQueryService = partDefinitionQueryService;
        this.productDefinitionPartsQueryService = productDefinitionPartsQueryService;
    }


    @RequestMapping(value = "/productDefinition", method = RequestMethod.GET)
    public String showAllProducts (@ModelAttribute("productDefinition") ProductDefinition productDefinition,
//                                   @ModelAttribute("productId") Long productId,
                                   Model model){
        LOGGER.debug("is executed!");
        model.addAttribute("productsDefinition", productDefinitionQueryService.findAll());
        model.addAttribute("productDefinition", productDefinition);
//        model.addAttribute("productId",productId);
        return "productDefinition";
    }

    @RequestMapping(value = "/productDefinition/save", method = RequestMethod.POST)
    public String saveProducts (@Valid @ModelAttribute("productDefinition") ProductDefinition productDefinition,
                                BindingResult bindingResult, RedirectAttributes redirectAttributes){
        LOGGER.debug("is executed!");
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "product", bindingResult);
            redirectAttributes.addFlashAttribute("flash" + "product", productDefinition);

            return "redirect:/productDefinition";
        }

        if (productDefinition.getId() == null) {
            productDefinitionCommandService.saveProductDefinition(productDefinition);
        } else {
            productDefinitionCommandService.update(productDefinition);
        }

        return "redirect:/productDefinition";
    }

    @RequestMapping(value = "/productDefinition/edit/{id}")
    public String getProduct (@PathVariable("id") Long id, Model model){
        LOGGER.debug("is executed!");
        model.addAttribute("productsDefinition", productDefinitionQueryService.findAll());
        model.addAttribute("productDefinition", productDefinitionQueryService.findById(id));

        return "productDefinition";
    }

    @RequestMapping(value = "/productDefinition/delete/{id}")
    public String deleteProduct (@PathVariable("id") Long id){
        LOGGER.debug("is executed!");
        productDefinitionCommandService.delete(id);
        return "redirect:/productDefinition";
    }

    @RequestMapping(value = "/productDefinition/partsToAdd/{id}")
    public String getPartsToAdd (@PathVariable("id") Long id, Model model){
//    public String getPartsToAdd (Model model){
        LOGGER.debug("is executed!");
        model.addAttribute("productDefinitionParts", productDefinitionPartsQueryService.findAll(id));
        return "partsToAdd";
    }
}
