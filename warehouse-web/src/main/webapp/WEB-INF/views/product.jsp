<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">

    <jsp:include page="common/menu.jsp"/>

    <div class="container">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col">Nazwa produktu</th>
                    <th scope="col">Opis</th>
                    <th scope="col" class="col-sm-3">Akcje</th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${!empty products}">
                        <c:forEach items="${products}" var="product">
                            <tr>
                                <td>${product.productName}</td>
                                <td>${product.productDescription}</td>
                                <td>
                                    <div class="btn-group">
                                        <spring:url value="/product/delete/${product.id}" var="deleteProduct"/>
                                        <button class="btn btn-info" onclick="location.href='${deleteProduct}'">
                                            Usuń
                                        </button>
                                        <spring:url value="/product/edit/${product.id}" var="editProduct"/>
                                        <button class="btn btn-info" onclick="location.href='${editProduct}'">
                                            Edytuj
                                        </button>
                                        <spring:url value="/product/showParts/${product.id}" var="showPartsOfProduct"/>
                                        <button class="btn btn-info" onclick="location.href='${showPartsOfProduct}'">
                                            Lista części
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>
                                Lista jest pusta.
                            </td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                <tr>
                    <td>
                        Dodaj produkt
                    </td>
                </tr>
                <tr>
                    <spring:url var="saveAction" value="/product/save"/>

                    <form:form method="post" modelAttribute="product" action="${saveAction}" class="form-horizontal">

                        <form:hidden path="id"/>

                        <spring:bind path="productName">
                            <td class="form-group ${status.error ? 'has-error' : ''}">
                                <form:label path="productName" class="col-sm-2 control-label">
                                </form:label>
                                <div class="col-sm-10">
                                    <form:input path="productName" class="form-control"/>
                                    <form:errors path="productName" class="control-label"/>
                                </div>
                            </td>
                        </spring:bind>
                        <spring:bind path="productDescription">
                            <td class="form-group ${status.error ? 'has-error' : ''}">
                                <form:label path="productDescription" class="col-sm-2 control-label">
                                </form:label>
                                <div class="col-sm-10">
                                    <form:input path="productDescription" class="form-control"/>
                                    <form:errors path="productDescription" class="control-label"/>
                                </div>
                            </td>
                        </spring:bind>
                        <td>
                            <button class="btn btn-primary" type="submit">
                                Zapisz
                            </button>
                        </td>

                    </form:form>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="container">

    </div>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
