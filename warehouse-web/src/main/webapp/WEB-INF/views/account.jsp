<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">

    <jsp:include page="common/menu.jsp"/>

    <div class="container">

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col">Login</th>
                    <th scope="col">Password</th>
                    <th scope="col" class="col-sm-3">Rola</th>
                    <th scope="col" class="col-sm-2"><spring:message code="part.actions"/></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${!empty accounts}">
                        <c:forEach items="${accounts}" var="account">
                            <tr>
                                <td>${account.login}</td>
                                <td>***</td>
                                <td>${account.accountType}</td>
                                <td>
                                    <spring:url value="/account/edit/${account.id}" var="accountEditUrl"/>
                                    <spring:url value="/account/delete/${account.id}" var="accountDeleteUrl"/>
                                    <div class="btn-group">
                                        <button class="btn btn-info" onclick="location.href='${accountEditUrl}'">
                                            Edytuj
                                        </button>
                                        <button class="btn btn-info" onclick="location.href='${accountDeleteUrl}'">
                                            Usuń
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>
                                Lista jest pusta
                            </td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                <tr>
                    <td>
                        <h2>Nowe konto</h2>
                    </td>
                </tr>
                <tr>
                    <spring:url var="saveAction" value="/account/save"/>

                    <form:form method="post" modelAttribute="account" action="${saveAction}" class="form-horizontal">

                        <form:hidden path="id"/>

                        <spring:bind path="login">
                            <td class="form-group ${status.error ? 'has-error' : ''}">
                                    <%--<form:label path="login" class="col-sm-0 control-label">--%>
                                    <%--</form:label>--%>
                                <div class="col-sm-10">
                                    <form:input path="login" class="form-control" placeholder="login"/>
                                    <form:errors path="login" class="control-label"/>
                                </div>
                            </td>
                        </spring:bind>

                        <spring:bind path="password">
                            <td class="form-group ${status.error ? 'has-error' : ''}">
                                    <%--<form:label path="password" class="col-sm-0 control-label">--%>
                                    <%--</form:label>--%>
                                <div class="col-sm-10">
                                    <form:input path="password" class="form-control" placeholder="password"/>
                                    <form:errors path="password" class="control-label"/>
                                </div>
                            </td>
                        </spring:bind>


                        <spring:bind path="accountType">
                            <td class="form-group">
                                <form:select path="accountType" items="${accountTypeList}"/>
                            </td>
                        </spring:bind>

                        <%--<form:select path="accountType" items="${accountTypeList}">--%>
                        <%--&lt;%&ndash;<form:option value="-" label="select"/>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<form:options items="${accoutType}"/>&ndash;%&gt;--%>
                        <%--</form:select>--%>

                        <%--<select id="accountType" name="accountType">--%>
                        <%--<option value="NONE">--- Select ---</option>--%>
                        <%--<option value="ADMIN">Admin</option>--%>
                        <%--<option value="WAREHOUSEMAN">Magazynier</option>--%>
                        <%--<option value="ENGINEER">Inzynier</option>--%>
                        <%--<option value="OTHER">Inny</option>--%>
                        <%--</select>--%>


                        <td>
                            <button class="btn btn-primary" type="submit">
                                Zapisz
                            </button>
                        </td>

                    </form:form>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="container">


    </div>

</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
