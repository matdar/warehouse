<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">

    <jsp:include page="common/menu.jsp"/>

    <div class="container">

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col"><spring:message code="part.partName"/></th>
                    <th scope="col"><spring:message code="part.symbol"/></th>
                    <th scope="col"><spring:message code="part.unit"/></th>
                    <th scope="col" class="col-sm-2"><spring:message code="part.actions"/></th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${partsDefinition}" var="part">
                    <tr>
                        <td>${part.name}</td>
                        <td>${part.symbol}</td>
                        <td>${part.unit}</td>
                        <td>
                            <spring:url value="/partDefinition/edit/${part.id}" var="partEditUrl"/>
                            <spring:url value="/partDefinition/delete/${part.id}" var="partDeleteUrl"/>
                            <div class="btn-group">
                                <button class="btn btn-info" onclick="location.href='${partEditUrl}'">
                                    <spring:message code="part.edit"/>
                                </button>
                                <button class="btn btn-info" onclick="location.href='${partDeleteUrl}'">
                                    <spring:message code="part.delete"/>
                                </button>
                            </div>
                        </td>
                    </tr>
                </c:forEach>



                <spring:url var="saveAction" value="/partDefinition/save"/>
                <form:form method="post" modelAttribute="partDefinition" action="${saveAction}">
                    <form:hidden path="id"/>
                    <tr>
                        <td>
                            <form:input type="text" name="Nazwa czesci" path="name"/><br>
                        </td>
                        <td>
                            <form:input type="text" name="Symbol" path="symbol"/><br>
                        </td>
                        <td>
                            <form:input type="text" name="Jednostka" path="unit"/><br>
                        </td>
                        <td>
                            <button class="btn btn-primary" type="submit">
                                <spring:message code="part.addPart"/>
                            </button>
                        </td>
                    </tr>
                </form:form>
                </tbody>
            </table>
        </div>
    </div>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>