<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<spring:url var="mainUrl" value="/main"/>

<button class="btn btn-primary" onclick="location.href = '${mainUrl}';">
    Warehouse
</button>
