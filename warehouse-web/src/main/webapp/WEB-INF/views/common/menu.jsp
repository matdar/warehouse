<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="container">
    <spring:url var="logoutUrl" value="/perform_logout"/>
    <form:form method="post" action="${logoutUrl}">
        <div class="btn-group">
            <spring:url var="mainUrl" value="/main"/>
            <spring:url var="partUrl" value="/part"/>
            <spring:url var="partDefUrl" value="/partDefinition"/>
            <spring:url var="productUrl" value="/product"/>
            <spring:url var="productDefinitionUrl" value="/productDefinition/"/>
            <spring:url var="accountUrl" value="/account/"/>
            <button  class="btn btn-primary" type="submit">Wyloguj</button>
            <button onclick="location.href = '${mainUrl}';"  type="button" class="btn btn-primary">Warehouse</button>
            <button onclick="location.href = '${partUrl}';" type="button" class="btn btn-primary" >Część</button>
            <button onclick="location.href = '${partDefUrl}';" type="button" class="btn btn-primary" >Część definicja</button>
            <button onclick="location.href = '${productUrl}';" type="button" class="btn btn-primary" >Produkt</button>
            <button onclick="location.href = '${productDefinitionUrl}';" type="button" class="btn btn-primary" >Produkt definicja</button>
            <button onclick="location.href = '${accountUrl}';" type="button" class="btn btn-primary" >Konta</button>

        </div>
    </form:form>
</div>

<%--<spring:url var="logoutUrl" value="/perform_logout"/>--%>
<%--<form:form method="post" action="${logoutUrl}">--%>
<%--<button  class="btn btn-primary" type="submit">--%>
<%--Wyloguj--%>
<%--</button>--%>
<%--</form:form>--%>




<%--<div class="row">--%>
<%--<div class="col-lg-1">--%>
<%--<jsp:include page="logout.jsp"/>--%>
<%--</div>--%>
<%--<div class="col-lg-1">--%>
<%--<jsp:include page="goToMainPage.jsp"/>--%>
<%--</div>--%>
<%--</div>--%>
