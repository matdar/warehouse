<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">

    <jsp:include page="common/menu.jsp"/>

    <div class="container">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col">Nazwa produktu</th>
                    <th scope="col">Opis</th>
                    <th scope="col">Części</th>
                    <th scope="col" class="col-sm-3">Akcje</th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${!empty productsDefinition}">
                        <c:forEach items="${productsDefinition}" var="productDefinition">
                            <tr>
                                <td>${productDefinition.name}</td>
                                <td>${productDefinition.description}</td>
                                <td></td>
                                <%--<td>${productDefinition.productDefParts}</td>--%>

                                <td>
                                    <div class="btn-group">
                                        <spring:url value="/productDefinition/delete/${productDefinition.id}" var="deleteProduct"/>
                                        <button class="btn btn-info" onclick="location.href='${deleteProduct}'">
                                            Usuń
                                        </button>
                                        <spring:url value="/productDefinition/edit/${productDefinition.id}" var="editProduct"/>
                                        <button class="btn btn-info" onclick="location.href='${editProduct}'">
                                            Edytuj
                                        </button>
                                        <spring:url value="/productDefinition/partsToAdd/${productDefinition.id}" var="partsToAdd"/>
                                        <button class="btn btn-info" onclick="location.href='${partsToAdd}'">
                                            Dodaj części
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>
                                Lista jest pusta.
                            </td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                <tr>
                    <td>
                        Dodaj produkt
                    </td>
                </tr>
                <tr>
                    <spring:url var="saveAction" value="/productDefinition/save"/>
                    <form:form method="post" modelAttribute="productDefinition" action="${saveAction}">
                        <form:hidden path="id"/>
                <tr>
                    <td>
                        <form:input type="text" name="Nazwa produktu" path="name"/><br>
                    </td>
                    <td>
                        <form:input type="text" name="Opis" path="description"/><br>
                    </td>
                    <td>

                    </td>
                    <td>
                        <button class="btn btn-primary" type="submit">
                            <spring:message code="product.addproduct"/>
                        </button>
                    </td>
                </tr>
                </form:form>
                </tbody>
            </table>
        </div>
    </div>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>