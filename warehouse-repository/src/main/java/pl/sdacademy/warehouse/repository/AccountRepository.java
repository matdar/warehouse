package pl.sdacademy.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sdacademy.warehouse.domain.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
}
