package pl.sdacademy.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionParts;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionPartsId;


@Repository
public interface ProductDefinitionPartsRepository extends JpaRepository<ProductDefinitionParts, Long> {

    ProductDefinitionParts findByProductDefinitionPartsId(ProductDefinitionPartsId productDefinitionPartsId);
}
